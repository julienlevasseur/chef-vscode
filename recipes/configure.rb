#
# Cookbook:: chef-vscode
# Recipe:: configure
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
	node.override['vscode']['folder']['settings']['user'] = "/home/#{node['vscode']['owner']}/.config/#{node['vscode']['flavor']}/User/"
  apt_update
  package 'git'

  git '/opt/Menlo-for-Powerline' do
    repository 'https://github.com/abertsch/Menlo-for-Powerline.git'
    revision 'master'
    action :sync
  end

  directory node['vscode']['menlo']['fonts_dir'] do
    owner node['vscode']['owner']
    group node['vscode']['group']
  end

  link "#{node['vscode']['menlo']['fonts_dir']}/#{node['vscode']['menlo']['font']}" do
    to "/opt/Menlo-for-Powerline/#{node['vscode']['menlo']['font']}"
  end
elsif node['platform'] == 'mac_os_x'
	node.override['vscode']['folder']['settings']['user'] = "/Users/#{node['vscode']['owner']}/Library/Application Support/#{node['vscode']['flavor']}/User/"
end

unless node['vscode']['extensions'].empty?
  node['vscode']['extensions'].each do |extension|
    vscode_extension extension do
      action :install
      cmd  node['vscode']['bin']
      user node['vscode']['owner']
    end
  end
end

pretty_settings = Chef::JSONCompat.to_json_pretty(node['vscode']['settings'])

directory node['vscode']['folder']['settings']['user'] do
#directory node['vscode']['settings_folder']['user'] do
  recursive true
  owner node['vscode']['owner']                                               
  group node['vscode']['group']
  not_if node['vscode']['folder']['settings']['user'].nil?
end

# template "/Users/#{node['vscode']['owner']}/.config/VSCode/User/settings.json" do
# template "#{node['vscode']['settings_folder']['user'][node['platform']]}/settings.json" do
template "#{node['vscode']['folder']['settings']['user']}/settings.json" do
  source 'settings.json.erb'
  owner node['vscode']['owner']
  group node['vscode']['group']
  mode '0644'
  variables settings: pretty_settings
end

pretty_keybindings = Chef::JSONCompat.to_json_pretty(node['vscode']['keybindings'])

# template "/Users/#{node['vscode']['owner']}/.config/VSCode/User/keybindings.json" do
# template "#{node['vscode']['settings_folder']['user'][node['platform']]}/keybindings.json" do
template "#{node['vscode']['folder']['settings']['user']}/keybindings.json" do
  source 'keybindings.json.erb'
  owner node['vscode']['owner']
  group node['vscode']['group']
  mode '0644'
  variables keybindings: pretty_keybindings
end
