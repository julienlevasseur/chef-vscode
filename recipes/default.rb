#
# Cookbook:: vscode
# Recipe:: default
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

include_recipe 'vscode::install'
include_recipe 'vscode::configure'