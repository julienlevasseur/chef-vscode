#
# Cookbook:: chef-vscode
# Recipe:: install
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
  apt_update

  %w(
    gnupg
    libasound2
    libnotify4
    libnss3
    libxkbfile1
    libgconf-2-4
    libsecret-1-0
    libgtk-3-0
    libxss1
  ).each do |pkg|
    package pkg
  end

  # Using: https://github.com/VSCodium/vscodium

  # remote_file '/tmp/vscode.deb' do
  remote_file "/tmp/vscodium-#{node['vscode']['version']}.deb" do
    source "#{node['vscode']['download_url']}/" + node['vscode']['version'].split('-')[0] + "/codium_#{node['vscode']['version']}_amd64.deb"
    mode '0644'
    not_if "dpkg -l|grep \"codium\"|grep #{node['vscode']['version']}"
  end

  dpkg_package "/tmp/vscodium-#{node['vscode']['version']}.deb" do
    action :install
    not_if "dpkg -l|grep \"codium\"|grep #{node['vscode']['version']}"
  end
end
