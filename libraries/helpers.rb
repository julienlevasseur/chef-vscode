#
# Chef Documentation
# https://docs.chef.io/libraries.html
#

class Chef
  class Recipe
    module Vscode
      require 'open3'

      module Extension
        def self.exist?(cmd, name, user)
          cmd = "su - #{user} -c \"#{cmd} --list-extensions|grep -i #{name}\""
          _stdin, stdout, _stderr = Open3.popen3(cmd)
          cmd_out = stdout.readlines
          if cmd_out.length != 0
            return true if cmd_out[0].include? name
          else
            return false
          end
        end

        def self.install(cmd, name, user)
          cmd = "su - #{user} -c \"#{cmd} --install-extension #{name}\""
          _stdin, stdout, _stderr = Open3.popen3(cmd)
          cmd_out = stdout.readlines
          success = false
          cmd_out.each do |line|
            success = true if line.include? 'was successfully installed!'
          end
          return success
        end

        def self.uninstall(cmd, name, user)
          cmd = "su - #{user} -c \"#{cmd} --uninstall-extension #{name}\""
          _stdin, stdout, _stderr = Open3.popen3(cmd)
          cmd_out = stdout.readlines
          success = false
          cmd_out.each do |line|
            success = true if line.include? 'was successfully uninstalled!'
          end
          return success
        end
      end
    end
  end
end

#
# This module name was auto-generated from the cookbook name. This name is a
# single word that starts with a capital letter and then continues to use
# camel-casing throughout the remainder of the name.
#
# module ChefVscode
#   module HelpersHelpers
#     #
#     # Define the methods that you would like to assist the work you do in recipes,
#     # resources, or templates.
#     #
#     # def my_helper_method
#     #   # help method implementation
#     # end
#   end
# end

#
# The module you have defined may be extended within the recipe to grant the
# recipe the helper methods you define.
#
# Within your recipe you would write:
#
#     extend ChefVscode::HelpersHelpers
#
#     my_helper_method
#
# You may also add this to a single resource within a recipe:
#
#     template '/etc/app.conf' do
#       extend ChefVscode::HelpersHelpers
#       variables specific_key: my_helper_method
#     end
#
