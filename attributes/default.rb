default['vscode']['flavor'] = "VSCodium" # Choose VSCode, Code or VSCodium
# VSCode or VSCodium for Ubuntu/Debian

default['vscode']['bin_by_flavor'] = {
  'VSCodium': {
    'ubuntu': '/usr/bin/codium',
    'debian': '/usr/bin/codium',
  },
  'VScode': {
    'ubuntu': '/usr/bin/code',
    'debian': '/usr/bin/code',
    'mac_os_x': '/Applications/Visual Studio Code.app/Contents/Resources/app/bin/code',
  }
}

default['vscode']['bin'] = node['vscode']['bin_by_flavor'][node['vscode']['flavor']][node['platform']]

if node['platform'] == 'debian' || node['platform'] == 'ubuntu'
	default['vscode']['folder']['settings']['user'] = "/home/#{node['vscode']['owner']}/.config/#{node['vscode']['flavor']}/User/"
elsif node['platform'] == 'mac_os_x'
	default['vscode']['bin'] = '/Applications/Visual Studio Code.app/Contents/Resources/app/bin/code'
	default['vscode']['folder']['settings']['user'] = ""
end

default['vscode']['settings_folder']['user'] = {
	"debian" => "/home/#{node['vscode']['owner']}/.config/#{node['vscode']['flavor']}/User/",
	"ubuntu" => "/home/#{node['vscode']['owner']}/.config/#{node['vscode']['flavor']}/User/",
	"mac_os_x" => "/Users/#{node['vscode']['owner']}/Library/Application Support/#{node['vscode']['flavor']}/User/",
}

default['vscode']['version'] = '1.56.2-1620951495'
default['vscode']['download_url'] = 'https://github.com/VSCodium/vscodium/releases/download'

default['vscode']['menlo']['fonts_dir'] = ''
default['vscode']['menlo']['font']      = 'Menlo for Powerline.ttf'

default['vscode']['owner']       = ''
default['vscode']['extensions']  = []
default['vscode']['settings']    = {}
default['vscode']['keybindings'] = []
