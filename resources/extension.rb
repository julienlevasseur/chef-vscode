# To learn more about Custom Resources, see https://docs.chef.io/custom_resources.html
resource_name :vscode_extension
provides :vscode_extension

property :name, String, name_property: true
property :cmd, String, required: true
property :user, String, required: true

default_action :install

action :install do
  unless ::Chef::Recipe::Vscode::Extension.exist?(new_resource.cmd, new_resource.name, new_resource.user)
    converge_by "Installing Code extension '#{new_resource.name}'" do
      begin
        ::Chef::Recipe::Vscode::Extension.install(new_resource.cmd, new_resource.name, new_resource.user)
        Chef::Log.debug("Installing Code extension [#{new_resource.name}]")
      rescue
        Chef::Log.error("Failed to install Code extension #{new_resource.name}")
      end
    end
  end
end

action :uninstall do
  if ::Chef::Recipe::Vscode::Extension.exist?(new_resource.cmd, new_resource.name, new_resource.user)
    converge_by "Uninstalling Code extension '#{new_resource.name}'" do
      begin
          ::Chef::Recipe::Vscode::Extension.install(new_resource.cmd, new_resource.name, new_resource.user)
          Chef::Log.debug("Uninstalling Code extension [#{new_resource.name}]")
      rescue
        Chef::Log.error("Failed to uninstall Code extension #{new_resource.name}")
      end
    end
  end
end
